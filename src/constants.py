CONFIG_JSON = "./config/config.json"
LOG = "./logs/logs.txt"
TIMESTAMP_FORMAT = "%d-%m-%Y %H:%M:%S"

MESSAGES = {
    "INITIATE_SYNC": "Initiating folder Sync",
    "CONFIG_FOUND": "Configuration file found",
    "SYNC_OK": "Folders are in sync",
    "CHECK_CONFIG": "Please check your config file",
    "CONFIG_CREATED": "Configuration created successfully",
    "CONFIG_NOT_FOUND": "Configuration not found",
    "ERROR_SOURCE": "Source folder does not exist",
    "ERROR_REPLICA": "Replica folder does not exist",
    "ERROR_INTERVAL": "Interval must be Int"
}
