import os
import time
import hashlib
from constants import LOG, TIMESTAMP_FORMAT

def logger(message):
    """
    It prints a message to the console and appends the same message to a log file
    
    :param message: The message to be logged
    """
    print(message)
    current_time = time.strftime(TIMESTAMP_FORMAT, time.localtime())

    with open(LOG, "a") as file:
        file.write("["+ current_time +"] "+ message + "\n")


def file_comparer(file_one, file_two):    
    """
    It opens both files, reads them, and compares the MD5 hashes of the two files
    
    :param file_one: The first file to compare
    :param file_two: The file you want to compare to
    :return: True or False
    """
    # Opening the files in binary mode, reading them, and comparing the MD5 hashes of the two files.
    with open(file_one, "rb") as fileOne:
        with open(file_two, "rb") as fileTwo:
            if hashlib.md5(fileOne.read()).hexdigest() == hashlib.md5(fileTwo.read()).hexdigest():
                return True
            else:
                return False
        
    
def compare_folder_hash(source, replica):
    """
    It compares the files in the source folder to the files in the replica folder
    
    :param source: The source folder
    :param replica: The path to the replica folder
    :return: a boolean value.
    """
    # get all file in source
    files_in_source = os.listdir(source)
    # get all file in replica
    files_in_replica = os.listdir(replica)
    # compare 2 list
    if len(files_in_source) != len(files_in_replica):
        return False

    # Comparing the files in the source folder to the files in the replica folder.
    for file in files_in_source:
        if file in files_in_replica:
            if not file_comparer(os.path.join(source, file), os.path.join(replica, file)):
                return False
        else:
            return False
    return True