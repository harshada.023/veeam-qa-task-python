import os
import time
import json
from helpers import logger, file_comparer, compare_folder_hash
from constants import CONFIG_JSON, MESSAGES, TIMESTAMP_FORMAT


logger(MESSAGES["INITIATE_SYNC"])

# This is to check if the config.json file is exist or not. If it is exist, it will print "CONFIG_SUCCESS"
# and load the config.json file. If it is not exist, it will ask the user to input the source folder path,
# replica folder path, and replication interval.
if os.path.isfile(CONFIG_JSON):
    logger(MESSAGES["CONFIG_FOUND"])

    with open(CONFIG_JSON) as file:
        config_data = json.load(file)
        source = (config_data["source"])
        replica = (config_data["replica"])
        interval = (config_data["interval"])
else:
    logger(MESSAGES["CONFIG_NOT_FOUND"])

    # This is to ask the user to input the source folder path. If the source folder path is not exist, it will
    # print "ERROR_SOURCE" and exit the program.
    source = input("Enter your source folder path:")
    if not os.path.isdir(source):
        logger(MESSAGES["ERROR_SOURCE"])
        exit()

    # To ask the user to input the replica folder path. If the replica folder path is not exist, it will
    # print "ERROR_REPLICA" and exit the program.
    replica = input("Enter your replica folder path:")
    if not os.path.isdir(replica):
        logger(MESSAGES["ERROR_REPLICA"])
        exit()

    # This is to ask the user to input the replication interval. If the replication interval is not exist, it will
    # print "ERROR_INTERVAL" and exit the program.
    interval = int(input("Enter replication interval (in seconds):"))
    if not isinstance(interval, int):
        logger(MESSAGES["ERROR_INTERVAL"])
        exit()

    # This is to create a config.json file.
    with open(CONFIG_JSON, "w") as file:
        config_data = {
            "source": source,
            "replica": replica,
            "interval": int(interval)
        }
        json.dump(config_data, file)
    logger(MESSAGES["CONFIG_CREATED"])

# A loop that will run every configured interval. It will check if the folder and replica are the same. If they
# are the same, it will sleep for configured interval. If they are not the same, it will check if the folder and
# replica are exist. If they are exist, it will check the file hash in the folder and compare it with
# the replica. If the file hash is the same, it will count the sync. If the file hash is not the same,
# it will update the file in the replica. If the file is not in the folder, it will delete the file in
# the replica. If the file is not in the replica, it will copy the file from the folder to the
# replica. After that, it will sleep for configured interval.
while True:
    # Checking if the source and replica are the same. If they are the same, it will sleep for configured interval.
    if compare_folder_hash(source, replica):
        logger(MESSAGES["SYNC_OK"])
        time.sleep(interval)
        continue

    # This is to check if the source folder path is exist or not. If it is exist, it will print "LOCATION
    # AVAILABLE". If it is not exist, it will print "ERROR_SOURCE" and "CHECK_CONFIG" and break the loop.
    if os.path.isdir(source):
        logger(source + " : LOCATION AVAILABLE")
    else:
        logger(MESSAGES["ERROR_SOURCE"])
        logger(MESSAGES["CHECK_CONFIG"])
        break

    # This is to check if the replica folder path is exist or not. If it is exist, it will print "LOCATION
    # AVAILABLE". If it is not exist, it will print "ERROR_REPLICA" and "CHECK_CONFIG" and break the loop.
    if os.path.isdir(replica):
        logger(replica + ": LOCATION AVAILABLE")
    else:
        logger(MESSAGES["ERROR_REPLICA"])
        logger(MESSAGES["CHECK_CONFIG"])
        break

    # This is to check if the replication interval is an integer or not. If it is an integer, it will
    # print
    # "interval: CORRECT". If it is not an integer, it will print "ERROR_INTERVAL" and "CHECK_CONFIG" and
    # break the loop.
    if isinstance(interval, int):
        logger(str(interval) + " : interval: CORRECT")
    else:
        logger(MESSAGES["ERROR_INTERVAL"])
        logger(MESSAGES["CHECK_CONFIG"])
        break

    # To count the sync, update, and delete.
    files_in_sync_count = 0
    updated_files_count = 0
    deleted_files_count = 0

    # get all file in source
    files_in_source = os.listdir(source)
    # get all file in replica
    files_in_replica = os.listdir(replica)

    # Checking if the file in replica is in the folder. If it is in the folder, it will check if the file
    # hash is the same. If the file hash is the same, it will count the sync. If the file hash is not the
    # same, it will update the file in the replica. If the file is not in the folder, it will delete the
    # file in the replica.
    for file in files_in_replica:
        if file in files_in_source:
            if file_comparer(os.path.join(source, file), os.path.join(replica, file)):
                logger(f"{file} is up to date")
                files_in_sync_count += 1
            else:
                # This is to update the file in the replica.
                updated_files_count += 1
                os.remove(os.path.join(replica, file))
                os.system("cp " + os.path.join(source, file) + " " + replica)
                logger(f"{file} is updated")
        if file not in files_in_source:
            # This is to delete the file in the replica.
            logger(f"{file} is deleted")
            deleted_files_count += 1
            os.remove(os.path.join(replica, file))

    # This is to check if the file in the folder is in the replica. If it is not in the replica, it will
    # copy the file from the folder to the replica.
    for file in files_in_source:
        if file not in files_in_replica:
            updated_files_count += 1
            logger(f"{file} is copied")
            os.system("cp " + os.path.join(source, file) + " " + replica)

    # To print the time, sync, update, and delete.
    current_time = time.strftime(TIMESTAMP_FORMAT, time.localtime())
    logger(f"[{current_time}] In sync: {files_in_sync_count}; updated: {updated_files_count}; deleted: {deleted_files_count};")

    # sleep for configured interval
    time.sleep(interval)
